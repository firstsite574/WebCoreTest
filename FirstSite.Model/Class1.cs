﻿using Pdd.Sdk.Apis;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace FirstSite.Model
{
    public class Class1
    {
        public string getTest()
        {
            return "znq";
        }

        public async Task<string> GetPddDdkGoodsRecommend()
        {
            String _clientId = "b597aeda4ba04f5c8cdc8a2b527c1cbf";
            String _clientSecret = "1e47f5523797818f1dc14fcb70a75eed97e290c4";

            int _PageSize = 20;
            int _PageNo = 1;
            int ChannelType = 1;//进宝频道推广商品，0-1.9包邮, 1-今日爆款, 2-品牌好货,3-相似商品推荐,4-猜你喜欢,5-实时热销榜,6-实时收益榜,7-今日热销榜,默认值5
            int Offset = (_PageNo - 1) * _PageSize;//从多少位置开始请求；默认值 ： 0
            var req = new PddDdkGoodsRecommendGetRequest(_clientId, _clientSecret)
            {
                Limit = _PageSize,
                Offset = Offset,
                ChannelType = ChannelType
            };
            var rsp = await req.InvokeAsync();
            return JsonConvert.SerializeObject(rsp.List);
        }
    }
}
