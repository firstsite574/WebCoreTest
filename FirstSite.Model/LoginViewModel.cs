﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstSite.Model
{
    public class LoginViewModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string PassWord { get; set; }
    }
}
