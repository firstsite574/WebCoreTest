using Newtonsoft.Json;

namespace Pdd.Sdk
{
    public abstract class PddBaseResponse
    {
        [JsonProperty("request_id")]
        public string RequestId { get; set; }
        [JsonProperty("error_code")]
        public string ErrCode { get; set; }
        [JsonProperty("error_msg")]
        public string ErrMsg { get; set; }
        [JsonProperty("sub_code")]
        public string SubErrCode { get; set; }
        [JsonProperty("sub_msg")]
        public string SubErrMsg { get; set; }
    }
}