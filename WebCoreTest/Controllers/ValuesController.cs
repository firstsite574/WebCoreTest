﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using FirstSite.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Pdd.Sdk.Apis;

namespace WebCoreTest.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : Controller
    {
        String _appKey = "cb286495747063b424edbc13196a6ce2";
        String _appSecret = "ed9038d652fc4d0096216c3355411905";
        String _clientId = "b597aeda4ba04f5c8cdc8a2b527c1cbf";
        String _clientSecret = "1e47f5523797818f1dc14fcb70a75eed97e290c4";

        // GET api/values
        [HttpGet]
        public ActionResult<string> Get()
        {
            Class1 cl = new Class1();

            return cl.getTest();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// 两数相加
        /// </summary>
        /// <param name="num1">第一个数</param>
        /// <param name="num2">第二个数</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<int> Sum(int num1, int num2)
        {
            return num1 + num2;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpGet]
        public async Task<string> GetPddDdkGoodsRecommend()
        {
            String _clientId = "b597aeda4ba04f5c8cdc8a2b527c1cbf";
            String _clientSecret = "1e47f5523797818f1dc14fcb70a75eed97e290c4";

            int _PageSize = 20;
            int _PageNo = 1;
            int ChannelType = 1;//进宝频道推广商品，0-1.9包邮, 1-今日爆款, 2-品牌好货,3-相似商品推荐,4-猜你喜欢,5-实时热销榜,6-实时收益榜,7-今日热销榜,默认值5
            int Offset = (_PageNo - 1) * _PageSize;//从多少位置开始请求；默认值 ： 0
            //var req = new PddDdkGoodsRecommendGetRequest(_clientId, _clientSecret)
            var req = new PddDdkGoodsRecommendGetRequest()
            {
                Limit = _PageSize,
                Offset = Offset,
                ChannelType = ChannelType
            };
            var rsp = await req.InvokeAsync();
            return JsonConvert.SerializeObject(rsp);
        }

        [HttpPost]
        public async Task<string> PddDdkGoodsPidGenerate()
        {
            //String _clientId = "b597aeda4ba04f5c8cdc8a2b527c1cbf";
            //String _clientSecret = "1e47f5523797818f1dc14fcb70a75eed97e290c4";

            //var req = new PddDdkGoodsPidGenerateRequest(_clientId, _clientSecret)
            var req = new PddDdkGoodsPidGenerateRequest()
            {
                Number = 1,
                PIdNameList = "[\"4\"]"
            };
            var rsp = await req.InvokeAsync();
            return JsonConvert.SerializeObject(rsp);
        }
    }
}
